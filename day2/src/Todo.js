import React, { Component } from "react";
import 'antd/dist/antd.css';
import { Button, Card, Input, Icon } from 'antd';

import "./Todo.css";    

function remove(data, index) {
    return [
        ...data.slice(0, index),
        ...data.slice(index + 1, data.length)
    ]
}

class Todo extends Component { 

    constructor(props){
        super(prop)
        this.state = {
            text: '',
            list: []
        }

       


    }      
    handleChange = (e) => {
        this.setState({
            text: e.target.value
        })
    }

    addTodo = (e) => {

    

        this.setState({
            text: '',
            list: this.state.list.concat(this.state.text)
        })
        console.log(this.state.list)  

    }
    
    deleteTodo = (index) => {
        this.setState({
            text: '',
            list: remove(this.state.list, index) 
        })
        console.log(this.state.list)
    }

  
    
    render() {
        return (
            <div>  
               <h1>Todo List</h1>
               <Card style={{ width: 300 }} className="BOX">      
               <Input value={this.state.text} onChange={this.handleChange}/>                          
               <Button type="primary" className="button-add" onClick={this.addTodo}>ADD TODO</Button>
               </Card>
             
               <div className="TodoLoop">{
                   this.state.list.map((v, i) =><Card className="card-todo"  extra={<a  onClick={() => this.deleteTodo(i)} href="#">X</a>} style={{ width: 600 }} > <h4>{v}</h4></Card> )
               }</div>

            </div>
        );
      }
    }

export default Todo;
