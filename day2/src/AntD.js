import React, { Component } from "react";
import 'antd/dist/antd.css';
import { Button, Card, Input, Icon } from 'antd';
import './AntD.css'



class AntD extends Component {   

      
    
    render() {
        return (
            <div>  
               <h1>Welcome to my application</h1>
               <Card title="Sign in" style={{ width: 300 }} className="BOX">
               <h4>User name</h4>
               <Input/>
               <h4>Password</h4>
               <Input />
               <h4>Card content</h4>
               <Button type="primary">SIGN IN</Button>
               <br/>
               <a href="#">Forgot Password?</a>
               <hr/>
               <h5>New User</h5>
               <Button type="primary">SIGN UP</Button>
               </Card>

            </div>
        );
      }
    }

export default AntD;
