import React, { Component } from 'react';
import { BrowserRouter, Route} from 'react-router-dom'

//Components
import Home from './components/Home.js'
import Headers from './components/Header.js'


import Todo from './components/Todo.js'
import Todo2 from './components/Todo2'
import Todo3 from './components/Todo3'
import TodoFirebase from './components/Todo-Firebase'





class App extends Component {
  render() {
    return (
      <BrowserRouter>
      <div>
          <Headers/> 
          <Route exact path="/" component={Home}/>
          <Route path="/todo1" component={Todo}/>
          <Route path="/todo2" component={Todo2}/>
          <Route path="/todo3" component={Todo3}/>    
          <Route path="/todofirebase" component={TodoFirebase}/>    
     </div>
      </BrowserRouter>
    )
  }
}

export default App;