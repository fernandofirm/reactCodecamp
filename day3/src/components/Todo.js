import React, { Component } from "react";
import "antd/dist/antd.css";
import { Button, Card, Input } from "antd";
import styled from 'styled-components';

const StyleCPN = styled.div`
h1{
 
  text-align: center;
  font-size: 60px;
}

.BOX {
  margin: 0 auto;
  margin-bottom: 40px;
}

.button-add {
  margin-top: 20px;
  margin-left: 65px;
}



.scroll {
  margin: 0 auto;
  display: block;
  width: 600px;
  height: 400px;
  overflow: scroll;
}

h4 {
  text-align: center;
}

`

let url = `http://localhost:3001/todo`;

// function remove(data, index) {
//   return [...data.slice(0, index), ...data.slice(index + 1, data.length)];
// }

class Todo extends Component {
  constructor() {
    super();
    this.state = {
      text: "",
      list: []
    };

   

    this.getFetch();
  }

  getFetch = async () => {
    const result = await fetch(url);
    let data = await result.json();

    await this.setState({
      text: "",
      list: this.state.list.concat(data)
    });
    console.log(data)
  };

  handleChange = e => {
    this.setState({
      text: e.target.value
    });
  };

  addTodo = async e => {
    e.preventDefault();

    let data = { list: this.state.text };

    const res = await fetch(url, {
      method: "POST", // or 'PUT'
      body: JSON.stringify(data),
      headers: new Headers({
        "Content-Type": "application/json"
      })
    });

    const todo = await res.json();

    console.log("Add Result is", todo);

    this.setState({
      text: "",
      list: this.state.list.concat(todo)
    });
    console.log(this.state.list);
  };

  deleteTodo = async id => {
    let urlId = `http://localhost:3001/todo/${id}`;

    const result = await fetch(urlId, {
      method: "DELETE", // or 'PUT'
      headers: new Headers({
        "Content-Type": "application/json"
      })
    });

    console.log("Delete", result);

    console.log("id", id);

    this.setState({
      text: "",
      list: this.state.list.filter(item => item.id !== id)
    });
    console.log(this.state.list);
  };
  render() {
    return (
      <StyleCPN>
        <h1>To-do-List 1</h1>
        <form onSubmit={this.addTodo}>
        
          <Card style={{ width: 300 }} className="BOX">
            <Input value={this.state.text} onChange={this.handleChange} />
            <Button
              type="primary"
              className="button-add"
              onClick={this.addTodo}
            >
              ADD TODO
            </Button>
          </Card>
      
        </form>
        <div />
        <div className="TodoLoop">
        <div class="scroll">
          {this.state.list.map(v => (
            <Card
              key={v.id}
              className="card-todo"
              extra={
                <a  onClick={() => this.deleteTodo(v.id)}>
                  X
                </a>
              }
              style={{ width: 600 }}
            >
              <h4>{v.list}</h4>
            </Card>
          ))}
          </div>
        </div>
      </StyleCPN>
    );
  }
}

export default Todo;
