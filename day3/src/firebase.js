import * as firebase from 'firebase'


const config = {
    apiKey: "AIzaSyAsghWjEshhe8D3fx0JnYeyhc9N9Vv2i0I",
    authDomain: "todo-list-17eff.firebaseapp.com",
    databaseURL: "https://todo-list-17eff.firebaseio.com",
    projectId: "todo-list-17eff",
    storageBucket: "todo-list-17eff.appspot.com",
    messagingSenderId: "151461419692"
  };
  
  
  export const firebaseApp = firebase.initializeApp(config);
  export const database = firebase.database()