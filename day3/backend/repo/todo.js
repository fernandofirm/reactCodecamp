module.exports = {
  create,
  list,
  find,
  changeContent,
  remove,
  markComplete,
  markIncomplete
};

async function create(db, todolist) {
  let result = await db.execute(`insert into todolist (list) values (?)`, [
    todolist
  ]);
  return result;
}

function createEntity(row) {
  return {
    id: row.id,
    list: row.list,
    status: row.status
  };
}
async function list(db) {
  let [rows] = await db.execute(`select id,list from todolist`);
  return rows.map(createEntity);
}

async function find(db, id) {
  let [rows] = await db.execute(
    `select id,list,status from todolist where id = ?`,
    [id]
  );
  return createEntity(rows[0]);
}

function changeContent(db, listContent, id) {
  return db.execute(`update todolist set list = ? where id = ?`, [
    listContent,
    id
  ]);
}

async function remove(db, id) {
  return db.execute(`delete from todolist where id = ?`, [id]);
}

function markComplete(db, id) {
  return db.execute(`update todolist set status = 1 where id = ?`, [id]);
}

async function markIncomplete(db, id) {
  await db.execute(`update todolist set status = 0 where id = ?`, [id]);
}
