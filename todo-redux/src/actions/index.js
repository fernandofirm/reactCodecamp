import { ADD_TODO, REMOVE_TODO } from '../constants'

export function addTodo(state) {
    const action = {
        type: ADD_TODO,
        state
    }
    return action
}

export function removeTodo(state){
    const action = {
        type: REMOVE_TODO,
        state
    }
    return action
}