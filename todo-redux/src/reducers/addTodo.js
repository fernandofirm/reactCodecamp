import { ADD_TODO } from '../constant.js'

export default (state = [], action) => {
    switch(action.type) {
        case ADD_TODO:
        const { addTodo } = action;
        return addTodo;
    default:
        return state
    }
}