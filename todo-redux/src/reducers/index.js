import { combineReducers  } from 'redux'
import addTodo from './addTodo.js'
import removeTodo from './removeTodo.js'

export default combineReducers({
    addTodo,
    removeTodo
})