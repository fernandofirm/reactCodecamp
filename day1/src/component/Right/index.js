import React, { Component } from 'react';
import './Right.css';

class Right extends Component {

  constructor(props) {
    super(props)
  }

  render() {

    return (
      <div className="Right">
        <div>
          <div>
            <img src="http://icons.iconarchive.com/icons/uiconstock/socialmedia/512/Twitter-icon.png" />
            <span><button>เข้าสู่ระบบ</button></span>
          </div>
          <h1>ดูสิ่งที่เกิดขึ้นบนโลกในขณะนี้</h1>
          <h2>เข้าร่วมทวิตเตอร์วันนี้</h2>
          <div>
            <input placeholder="โทรศัพท์หรืออีเมล" />
          </div>
          <div>
            <input placeholder="รหัสผ่าน" type="password"/>
          </div>
          <div>
            <button>เริ่มต้นกันเลย</button>
            <span>มีบัญชีผู้ใช้อยู่แล้ว? <a> เข้าสู่ระบบ</a></span>
          </div>
        </div>
      </div>
    );
  }
}

export default Right;