import React, { Component } from 'react';
import { Form, Icon, Input, Button, Card } from 'antd';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      Username: '',
      Password: '',
      sum: ''
    }

    this.handleChangeUsername = this.handleChangeUsername.bind(this)
    this.handleChangePassword = this.handleChangePassword.bind(this)
    this.handleOnClick = this.handleOnClick.bind(this)
  }

  handleChangeUsername(event) {
    this.setState({ Username: event.target.value })
  }
  handleChangePassword(event) {
    this.setState({ Password: event.target.value })
  }

  handleOnClick(event) {
    let temp
    temp = parseInt(this.state.Username) + parseInt(this.state.Password)
    this.setState({ sum: temp })
  }

  render() {

    const FormItem = Form.Item;

    return (
      <div className="App">

        <h1>Welcome to my application</h1>
        <Card title="Sing In" style={{ width: 300, margin: 'auto' }}>
          <Form className="login-form">
            <FormItem>
              <Input value={this.state.Username} onChange={this.handleChangeUsername} prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
            </FormItem>
            <FormItem>
              <Input value={this.state.Password} onChange={this.handleChangePassword} prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
            </FormItem>
            <FormItem>
              <Button onClick={this.handleOnClick} type="primary" htmlType="submit" className="login-form-button">
                Sign in
                    </Button>
              <a className="login-form-forgot" href="">Forgot password</a>
            </FormItem>
            <hr></hr>
            <FormItem>
              <Button type="primary" htmlType="submit" className="login-form-button">
                Sign up
                    </Button>
            </FormItem>
          </Form>
          Total is {this.state.sum}
        </Card>
      </div>
    );
  }
}

export default App;